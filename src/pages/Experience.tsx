import { Component } from 'react';
import MainLayout from '../components/layouts/MainLayout';
import ExperienceCard from '../components/mollecules/ExperienceCard';

class ExperiencePage extends Component {
	render() {
		return (
			<MainLayout>
				<h2 className="text-center font-secondary">My Experience</h2>

				<div className="mt-8 space-y-10">
					<ExperienceCard position="Intern Frontend Developer" company="BangBeli" date="September 2022 - ongoing" type="work" />
					<ExperienceCard position="Frontend Developer Bootcamp" company="Hacktiv8 x Kampus Merdeka" date="August 2022 - ongoing" type="study" />
					<ExperienceCard position="Intern Frontend Developer" company="Zerolim" date="March 2022 - June 2022" type="study" />
				</div>
			</MainLayout>
		);
	}
}

export default ExperiencePage;
