import { Component } from 'react';
import MainLayout from '../components/layouts/MainLayout';

class SkillsPage extends Component {
	render() {
		return (
			<MainLayout>
				<h2 className="text-center font-secondary">My Skills</h2>

				<div className="mt-8">
					<h5 className="text-lg font-semibold">Programming Languanges</h5>
					<p className="tracking-wide text-slate-600">Javascript, Typescript, PHP</p>
				</div>
				<div className="mt-6">
					<h5 className="text-lg font-semibold">Technologies</h5>
					<p className="tracking-wide text-slate-600">React JS, Next.js, Jotai, Firebase, MySQL, Codeigniter, Tailwind CSS, Bootstrap</p>
				</div>
			</MainLayout>
		);
	}
}

export default SkillsPage;
