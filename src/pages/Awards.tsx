import { Component } from 'react';
import MainLayout from '../components/layouts/MainLayout';

class AwardsPage extends Component {
	render() {
		return (
			<MainLayout>
				<h2 className="text-center font-secondary">My Awards</h2>

				<div className="mt-8">
					<h5 className="text-lg font-semibold">Until now I have not received an award 🙂.</h5>
				</div>
			</MainLayout>
		);
	}
}

export default AwardsPage;
