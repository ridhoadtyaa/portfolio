import React, { Component } from 'react';
import MainLayout from '../components/layouts/MainLayout';

class AboutPage extends Component {
	render() {
		return (
			<MainLayout>
				<h2 className="text-center font-secondary">About Me</h2>

				<div className="mt-8">
					<p className="tracking-wide text-slate-600">I am Ridho Aditya Nurtama, a bachlecor of computer science student.</p>
					<p className="mt-4 tracking-wide text-slate-600">
						I'm a Front End Developer and an informatics engineering student from Universitas Mercu Buana. I'm currently focusing on web development,
						especially the front end and have knowledge of the back end. I'm passionate about Modern Web Development and always looking for new
						challenges and experiences.
					</p>
				</div>
			</MainLayout>
		);
	}
}

export default AboutPage;
