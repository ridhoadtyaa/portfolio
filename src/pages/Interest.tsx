import { Component } from 'react';
import MainLayout from '../components/layouts/MainLayout';

class InterestPage extends Component {
	render() {
		return (
			<MainLayout>
				<h2 className="text-center font-secondary">My Interest</h2>

				<div className="mt-8">
					<h5 className="text-lg font-semibold">Currently interested in Mobile development and Machine learning.</h5>
				</div>
			</MainLayout>
		);
	}
}

export default InterestPage;
