import React from 'react';
import MainLayout from '../components/layouts/MainLayout';
import { BsGithub, BsLinkedin } from 'react-icons/bs';
import { RiInstagramFill } from 'react-icons/ri';

class HomePage extends React.Component {
	render() {
		return (
			<MainLayout>
				<h2 className="text-center font-secondary">Ridho Aditya Nurtama</h2>
				<h3 className="mt-3 text-center font-secondary font-semibold">Student & Frontend Developer</h3>

				<div className="mx-auto mt-6 flex items-center justify-center space-x-5">
					<a href="https://github.com/ridhoadtyaa" target="_blank">
						<BsGithub color="#171515" size={22} />
					</a>
					<a href="https://www.linkedin.com/in/ridhoadtyaa/" target="_blank">
						<BsLinkedin color="#0072b1" size={22} />
					</a>
					<a href="https://www.instagram.com/ridhoadtyaa/" target="_blank">
						<RiInstagramFill color="#fb3958" size={24} />
					</a>
				</div>
			</MainLayout>
		);
	}
}

export default HomePage;
