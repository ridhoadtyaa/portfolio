import { Suspense, lazy, Component } from 'react';
import { Route, Routes } from 'react-router-dom';
import LoadingPage from './components/atoms/LoadingPage';

const HomePage = lazy(() => import('./pages/Home'));
const AboutPage = lazy(() => import('./pages/About'));
const ExperiencePage = lazy(() => import('./pages/Experience'));
const SkillsPage = lazy(() => import('./pages/Skills'));
const InterestPage = lazy(() => import('./pages/Interest'));
const AwardsPage = lazy(() => import('./pages/Awards'));

class RouteApp extends Component {
	render() {
		return (
			<Suspense fallback={<LoadingPage />}>
				<Routes>
					<Route path="/" element={<HomePage />} />
					<Route path="/about" element={<AboutPage />} />
					<Route path="/experience" element={<ExperiencePage />} />
					<Route path="/skills" element={<SkillsPage />} />
					<Route path="/interest" element={<InterestPage />} />
					<Route path="/awards" element={<AwardsPage />} />
				</Routes>
			</Suspense>
		);
	}
}

export default RouteApp;
