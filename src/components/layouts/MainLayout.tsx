import React, { Component } from 'react';
import Header from '../organism/Header';

type Props = {
	children: React.ReactNode;
};

class MainLayout extends Component<Props> {
	constructor(props: Props) {
		super(props);
	}

	render() {
		return (
			<>
				<Header />
				<main className="layout my-10 scroll-mt-10">{this.props.children}</main>
			</>
		);
	}
}

export default MainLayout;
