import { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Header extends Component {
	render() {
		return (
			<header className="layout flex flex-col items-center py-4">
				<h3 className="font-secondary font-semibold italic">Ridho Aditya</h3>
				<nav className="mt-6">
					<ul className="flex items-center space-x-6">
						<li>
							<NavLink end to="/" className={navData => (navData.isActive ? 'underline underline-offset-8' : '')}>
								Home
							</NavLink>
						</li>
						<li>
							<NavLink to="/about" className={navData => (navData.isActive ? 'underline underline-offset-8' : '')}>
								About
							</NavLink>
						</li>
						<li>
							<NavLink to="/experience" className={navData => (navData.isActive ? 'underline underline-offset-8' : '')}>
								Experience
							</NavLink>
						</li>
						<li>
							<NavLink to="/skills" className={navData => (navData.isActive ? 'underline underline-offset-8' : '')}>
								Skills
							</NavLink>
						</li>
						<li>
							<NavLink to="/interest" className={navData => (navData.isActive ? 'underline underline-offset-8' : '')}>
								Interest
							</NavLink>
						</li>
						<li>
							<NavLink to="/awards" className={navData => (navData.isActive ? 'underline underline-offset-8' : '')}>
								Awards
							</NavLink>
						</li>
					</ul>
				</nav>
			</header>
		);
	}
}

export default Header;
