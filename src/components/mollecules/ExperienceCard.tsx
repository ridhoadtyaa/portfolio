import { Component } from 'react';
import { MdWork } from 'react-icons/md';
import { GiGraduateCap } from 'react-icons/gi';

type Props = {
	position: string;
	company: string;
	date: string;
	type: 'work' | 'study';
};

class ExperienceCard extends Component<Props> {
	constructor(props: Props) {
		super(props);
	}

	render() {
		const { position, company, date, type } = this.props;
		return (
			<div>
				<h3>{position}</h3>
				<div className="mt-2 flex items-center space-x-2">
					{type === 'work' ? <MdWork /> : <GiGraduateCap size={22} />}
					<span>&mdash; {company}</span>
				</div>
				<p className="mt-1 text-sm text-slate-800">{date}</p>
			</div>
		);
	}
}

export default ExperienceCard;
